som_weight = som_grade = som_weighted_grades = count= 0


def askNumbers(message):
	while True:
		try:
			value = float(input(message))
			if message == "Nota:" and (value <= 10 and value >= 0):
				return value
			if message == "Peso:" and (value <= 5 and value >= 1):
				return int(value)
			print("valor errado inserido")
		except:
			print("valor errado inserido")

def somWeight(weight, som_weight):
	som_weight = som_weight + weight
	return som_weight

def somGrade(grade, som_grade):
	som_grade = som_grade + grade
	return som_grade

def somWeightedGrades(grade, weight, som_weighted_grades):
	som_weighted_grades = som_weighted_grades + (grade * weight)
	return som_weighted_grades

def counting(count):
	return count + 1

def askState():
	while True:
		try:
			state = input("deseja adicionar mais notas? [S/N] ")
			state = state.upper()
			if state == "S":
				return True
			if state == "N":
				return False
		except:
			print("valor errado inserido")

def printResults(som_grade, count, som_weighted_grades, som_weight):
	print("media =", som_grade/count)
	print("media ponderada =", som_weighted_grades/som_weight)
	return

calculing = True

while calculing:
	grade = askNumbers("Nota:")
	weight = askNumbers("Peso:")
	som_weight = somWeight(weight, som_weight)
	som_grade = somGrade(grade, som_grade)
	som_weighted_grades = somWeightedGrades(grade, weight, som_weighted_grades)
	count = counting(count)
	calculing = askState()

printResults(som_grade, count, som_weighted_grades, som_weight)