import math
import sys

def askName():
	name = input("digite seu nome completo: ")
	return name

def askHeight():
	height = float(input("digite sua altura: "))
	return height

def askNumberFriends():
	friends = int(input("digite o numero de acompanhantes: "))
	return friends

def validateString(name):
	return type(name) is str

def validateFloat(height):
	return type(height) is float

def validateInt(friends):
	return type(friends) is int
	
def validateName(name):
	valid = False
	name = len(name.split())
	if name > 2 and name < 6:
		valid = True
	return valid

def validateHeight(height):
	valid = False
	if height > 0.50 and height < 2.50:
		valid = True
	return valid

def validateFriends(friends):
	valid = False
	if friends < 10:
		valid = True
	return valid

def checkPermission(name, height, friends):
	valid = False
	if (len(name.split()) == 3 and height > 1.61 and friends > 0) or (height == 1.51 and friends == 4):
		valid = True
		print("pode andar na roda gigante!")
	return valid

def checkPrize(cabinNumber, prize):
	valid = False
	if cabinNumber >= 10 and Prize == 100:
		valid = True
		print("parabéns você ganhou o premio do palhaço louco")
	return valid

cabinNumber = 1
prize = 0

while True:

	userCount = 0
	# solicita nome para o usuário
	name    = askName()

	# solicita altura para o usuário
	height  = askHeight()

	# solicita quantidade de acompanhantes para o usuário
	friends = askNumberFriends()

	# verifica se o valor armazenado na variável name é uma string
	# e armazena o resultado em vs
	vs    = validateString(name)

	# verifica se o valor armazenado na variável height é um float
	# e armazena o resultado em vf
	vf    = validateFloat(height)

	# verifica se o valor armazenado na variável friends é um inteiro
	# e armazena o resultado em vi
	vi    = validateInt(friends)

	# checa validade das entradas
	if (not(vs and vf and vi)):
		print("Não pode!")
		sys.exit()


	# verifica se as entradas correspondem a realidade
	# do cenário apresentado
	vname    = validateName(name)
	vheight  = validateHeight(height)
	vfriends = validateFriends(friends)

	# chaca validade da realidade
	if (not(vname and vheight and vfriends)):
		print("Tá Errraado!")
		sys.exit()
		

	# verifica se o usuário pode utilizar a roda gigante
	allowed = checkPermission(name, height, friends)

	userCount = userCount + friends + 1


	if userCount >= 2:
		cabinNumber = cabinNumber + (math.ceil((friends + 1)/2))
	if cabinNumber >= 10:
		prize = prize + 1
		cabinNumber == cabinNumber - 10


	if not allowed:
		print("Bye bye!")
		sys.exit()

	# verifica se usuário ganhou o prêmio
	uhuu = checkPrize(cabinNumber, prize)