from dog import Dog

class Pug(Dog):
	def __init__(self, type_of_tail = "enrolado"):
		self._type_of_tail = type_of_tail

	@property
	def type_of_tail(self):
		return self._type_of_tail

	@type_of_tail.setter
	def type_of_tail(self, type_of_tail):
		self._type_of_tail = type_of_tail
	
	def yelp(self):
		print("au tshi au tshi")