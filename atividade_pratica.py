import sys

quantidade_notas = int(input("quantas notas? "))
vezes = 0
pesos = 0
media = 0
media_peso = 0

while vezes < quantidade_notas:
	nota = float(input("nota: "))
	if nota > 10 or nota < 0:
		sys.exit()
	peso = int(input("peso: "))
	if peso > 5 or peso < 1:
		sys.exit()
	pesos = pesos + peso
	media = media + nota
	media_peso = media_peso + (nota * peso)
	vezes = vezes + 1

media = media / quantidade_notas
media_peso = media_peso / pesos

print("media =", media, "media peso =", media_peso)