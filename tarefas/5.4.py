def askInt(message):
	value = int(input(message))
	return value

def countHalfyear(age, halfyear):
	if halfyear % 2 != 0:
		halfyear = halfyear + 1
	missingHalfyear = (8 - halfyear)/2
	return (age + missingHalfyear)

age = askInt("Quantos anos: ")
halfyear = askInt("quantos semestres: ")
result = countHalfyear(age, halfyear)
print(result)