cebolas = 250 #define a quantidade de cebolas
cebolas_na_caixa = 600 # define quantas cebolas estão dentro das caixas
espaco_caixa = 20 #define o espaço por caixa
caixas = 3 #define a quantidade de caixa
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa #define quantas cebolas estão fora da caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa) #define quandas caixas permaneceram vazias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa #define quantas caixas serão necessarias para empacotar o resto das cebolas
print('Existem', cebolas_na_caixa, 'cebolas encaixotadas') #mostra na tela quantas cebolas tem nas caixas
print('Existem', cebolas_fora_da_caixa, 'cebolas sem caixa') #mostra na tela quantas cebolas estão fora da caixa
print('Em cada caixa cabem', espaco_caixa, 'cebolas') #mostra na tela quantas cebolas cabem em uma caixa
print('Ainda temos,', caixas_vazias, 'caixas vazias') #mostra na tela quantas caixas permanecem vazias
print('Então, precisamos de', caixas_necessarias, 'caixas para empacotar todas as cebolas') #mostra na tela quantas caixas são necessarias para empacotar