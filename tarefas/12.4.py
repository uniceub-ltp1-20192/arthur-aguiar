buffet = ("Carne com molho madeira", "Batata assada", "Lasanha", "Arroz", "Salada")

for item in buffet:
	print(item)

try:
	buffet.append("feijão")
except:
	print("impossivel alterar tupla")

buffet_alterado = ("Strogonoff", "Batata frita", "Lasanha", "Arroz", "Salada")

print("os itens do novo cardapio são:")

for item in buffet_alterado:
	print(item)