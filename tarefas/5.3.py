def askValue(message):
	value = int(input(message))
	return value

def printResults(value1, value2):
	print("o resultado da adição é", value1 + value2)
	print("o resultado da subtração é", value1 - value2)
	print("o resultado da multiplicação é", value1 * value2)
	print("o resultado da divisão é", value1 / value2)

value1 = askValue("primeiro valor: ")
value2 = askValue("segundo valor: ")
printResults(value1, value2)