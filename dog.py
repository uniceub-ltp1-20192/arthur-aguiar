class Dog:
	def __init__(self, size = 0.0, race = "", color = "", weight = 0.0):
		self._size = size
		self._race = race
		self._color = color
		self._weight = weight

	@property
	def size(self):
		return self._size

	@size.setter
	def size(self, size):
		self._size = size
	
	@property
	def race(self):
		return self._race

	@race.setter
	def race(self, race):
		self._race = race

	@property
	def color(self):
		return self._color

	@color.setter
	def color(self, color):
		self._color = color

	@property
	def weight(self):
		return self._weight

	@weight.setter
	def weight(self, weight):
		self._weight = weight

	def yelp(self):
		print("au au")

	def eat(self):
		print("comendo")