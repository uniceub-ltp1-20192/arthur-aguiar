from pug import Pug
from dog import Dog

dog = Dog()
pug = Pug()

dog.size = 2.0
dog.race = "pug"
dog.color = "branco"
dog.weight = 10.0

dog.yelp()
dog.eat()
print(vars(dog))

pug.size = 2.0
pug.race = "pug"
pug.color = "branco"
pug.weight = 10.0

pug.yelp()
pug.eat()
print(vars(pug))